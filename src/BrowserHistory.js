export default class BrowserHistory {

    constructor(stack) {
        this._historial = stack
    }

    go(linkPage) {
        this._historial.push(linkPage)
    }

    back() {
        this._historial.pop()
    }

    get currentPage() {
        return this._historial.peek()
    }

}