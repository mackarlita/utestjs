export default class AuthorizationService {

    constructor() {
        this.roles = ['admin', 'manager', 'user'];
    }

    isAuthorized(role) {
        //TODO: Implement logic to validate that role is included on the valid roles list (this.roles)
    }

    isAdmin(role) {
        //TODO: Validate that role is an admin
    }
}