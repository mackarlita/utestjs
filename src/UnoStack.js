export default class UnoStack {

    constructor() {
        this._top = null
    }

    set top(node) {
        this._top = node
    }

    get top() {
        return this._top
    }

    isEmpty() {
        return this._top == null
    }

    peek() {
        if(this._top) {
            return this._top.value
        }
    }

    pop() {
        if (this.isEmpty()) {
            throw new EmptyStackException('UnoStack is empty')
        }

        const value = this.top.value
        this.top = this.top.next
        return value
    }

    push(newValue) {
        let node = new Node(newValue)
        node.next = this._top
        this._top = node
    }

}

class Node {

    constructor(data) {
        this._value = data
        this._next = null
    }

    set value(data) {
        this._value = data
    }

    get value() {
        return this._value
    }

    set next(node) {
        this._next = node
    }

    get next() {
        return this._next
    }

}

class EmptyStackException extends Error {}