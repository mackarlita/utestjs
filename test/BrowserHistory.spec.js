import { expect } from 'chai'
import sinon from 'sinon'

import BrowserHistory from '../src/BrowserHistory'

describe('BrowserHistory', () => {

    let bhistory, stack = null;
    before(() => {
        stack = {
            push: sinon.spy()
        }
        bhistory = new BrowserHistory(stack)
    })

    context('#go()', () => {
        it('Should store url into stack', () => {
            const testUrl = 'test/url'
            bhistory.go(testUrl)
            expect(stack.push.calledWith(testUrl)).to.be.true
        })
    })

    context.skip('#back()', () => {
        it('Should take out the current page from the stack', () => {

        })
    })

    context.skip('#currentPage()', () => {
        it('Should retrieve the url at the top of the stack', () => {

        })
    })


})