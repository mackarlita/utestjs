import { expect } from 'chai'

import UnoStack from '../src/UnoStack'

describe('UnoStack', function () {

    let stack = null
    before(function() {
        stack = new UnoStack()
    })

    context('Stack initialization', function() {
        it('should make sure stack is empty', function() {
            expect(stack.isEmpty())
        })
    })

    context('Stack is empty', function() {

        it('#isEmpty() should be true')

        it('#peek() should return null')

        it('#pop() should thrown an EmptyStackException')
    })

    context.skip('Stack contains elements', function() {

    })

})