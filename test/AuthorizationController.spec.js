import { assert } from 'chai'

import AuthorizationService from '../src/AuthorizationService'

describe('AuthorizationService', () => {

    let authCtrl = null;
    before(() => {
        authCtrl = new AuthorizationService()
    })


    describe('#isAuthorized()', () => {

        context('When role is contained into valid roles list', () => {
            it('should authorize the access', () => {
                assert.fail()
            })
        })

        context('When role is not into valid roles list', () => {
            it('should denegate the access')
        })
    })

    describe('#isAdmin()', () => {

        it('Should be true when role is an admin')

        it('Should be false when role is not an admin')
    })

})